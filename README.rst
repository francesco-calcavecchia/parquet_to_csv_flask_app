==============
parquet_to_csv
==============


Flask web application to convert parquet file into csv.


Run the web app
===============

.. code-block::

  export PYTHONPATH="$(pwd)/src/parquet_to_excel"
  export FLASK_APP=src/paruqet_to_excel/app.py
  export FLASK_ENV=development
  flask run


Note
====

This project has been set up using PyScaffold 3.2.3. For details and usage
information on PyScaffold see https://pyscaffold.org/.
