import io

import pandas as pd
from flask import Flask, render_template, send_file
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from flask_wtf import FlaskForm
from wtforms import FileField, SubmitField
from wtforms.validators import DataRequired

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mypassword'

bootstrap = Bootstrap(app)
moment = Moment(app)


class FileForm(FlaskForm):
    file = FileField(label='Choose the file to convert', validators=[DataRequired()])
    submit = SubmitField('Convert to CSV')


@app.route('/', methods=['GET', 'POST'])
def index():
    form = FileForm()
    if form.validate_on_submit():
        df = pd.read_parquet(form.file.data)
        file = io.BytesIO()
        file.write(bytes(df.to_csv(), 'utf-8'))
        file.seek(0)
        return send_file(filename_or_fp=file, mimetype='text/csv',
                         as_attachment=True, attachment_filename='{}.csv'.format(form.file.name))
    return render_template('index.html', form=form)
